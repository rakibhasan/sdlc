package com.maxim.jms.adapter;

import java.net.UnknownHostException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {
	
	public void sendToMongo(String json) throws UnknownHostException {
		System.out.println("Sending to MongoDB");
		MongoClient client = new MongoClient();
		DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("contact");
		System.out.println("Converting JSON to DBOject");
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
		System.out.println("Sent to MongoDB");
	}

}
