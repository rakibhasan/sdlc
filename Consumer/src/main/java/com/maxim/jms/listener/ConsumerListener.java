package com.maxim.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {
	
	@Autowired
	JmsTemplate jmsTemplate;
	@Autowired
	ConsumerAdapter consumerAdapter;

	public void onMessage(Message message) {
	
		String json = null;

		if (message instanceof TextMessage) {
			try {
				json = ((TextMessage) message).getText();
				
				System.out.println("Sending JSON to DB: " + json);
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				System.out.println("Message: " + json);
				jmsTemplate.convertAndSend(json);
			} catch (UnknownHostException e) {
				System.out.println("Message: " + json);
				jmsTemplate.convertAndSend(json);
			} catch (Exception e) {
				System.out.println("Message: " + json);
				jmsTemplate.convertAndSend(json);
			}
		}
	}

}
